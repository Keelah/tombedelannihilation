---
share: "true"
---
![Valdryzel](./Valdryzel.jpg)
## Caractéristiques 

**Race :** Centaure
**Historique :** Sauvageon
**Classe :** Barbare
**Joueur :** [Keelah](./Keelah.md)

## Inventaire

**Arme :** Hache du Jugement Spectral

## Lore

Valdryzel "Val" Oakenhoof est née au sein d'une tribu de centaures dans les plaines verdoyantes de Dales. Son grand-père avait gagné le nom d'Oakenhoof en étant le premier centaure de la tribu à avoir une jambe de bois et à avoir repoussé a lui seul des bandits. Il a toujours été un modèle pour Val de par sa bravour et son courage, mais aussi sa bonté.

Après des années rudes a errer et survivre, la tribu parvint à s'allier à des fermiers elfes pour former une véritable communauté. Les centaures fournissaient de la main-d'œuvre et de la protection tandis que les elfes fournissaient de la nourriture, des maisons et des artisanats.

  
La vie était belle et paisible pendant toute son enfance où Val pu grandir et devenir un combattant comme les membres de sa famille et protéger la communauté qui devenait de plus en plus grandissante et fructueuse.

Val s'est battue pour sa ferme après qu'un groupe d'orcs les a attaqués pour piller leur richesse et asservir les gens. Malheureusement, elle n'était pas assez forte pour les combattre. Toute la ferme a été détruite et tous les ouvriers agricoles ont été tués ou emmenés alors qu'elle était laissée pour morte au milieu du champ de bataille. Cette tragédie l'a profondément traumatisée et l'a conduit à l'alcoolisme et a faire des cauchemars très régulièrement.

Vouée à elle meme, elle parcours désormais le monde pour s'entraîner, jurant de devenir suffisamment forte pour vaincre tous les pillards qu'elle pourrait rencontrer.  
  
Depuis, Val a dû faire face à des difficultés pour joindre les deux bouts. Sans abri ni ressources, elle a dû se débrouiller seule dans un monde impitoyable. Sa dépendance à l'alcool ne faisait qu'aggraver ses problèmes, car une grande partie de son maigre revenu était dépensée pour satisfaire sa soif insatiable.

Elle a parcouru le monde en tant que mercenaire, offrant ses compétences en tant que combattante et protectrice. Mais la vie n'a pas été tendre avec elle. Elle a été trahie par des clients qui ont refusé de payer, blessée par des adversaires plus forts et a même été capturée et vendue comme esclave une fois.  
En tant que centaure, il est difficile pour elle de trouver des emplois qui correspondent à ses compétences et à sa force physique. Elle a essayé de travailler comme garde du corps ou comme mercenaire, mais les emplois étaient rares et les paiements insuffisants.

Malgré ces revers, Val refuse de baisser les bras. Elle reste déterminée à changer son destin et à se prouver qu'elle peut surmonter tous les obstacles qui se dressent sur son chemin. Ses compétences de combat, héritées de son grand-père, sont impressionnantes, et elle sait qu'elle a la force nécessaire pour se défendre. Cependant, la vie continue de lui donner des coups durs.  
Les rencontres avec des bandits, des monstres errants et d'autres adversités sont devenues monnaie courante pour Val. Chaque victoire est suivie d'une nouvelle épreuve qui met ses compétences à rude épreuve. Elle reste toujours en alerte, prête à combattre, mais elle ne peut s'empêcher de ressentir une pointe de frustration face aux difficultés qui semblent ne jamais prendre fin.  
  
  
Note :  
Elle vit avec le souvenir constant qu'elle n'a pas pu sauver sa famille et ses amis et a depuis trouvé refuge dans le whiskey, le rhum et la bière. Cela a considérablement augmenté sa tendance à frapper tout ce qui bouge.




### Avant de tout perdre
- 371 PO
- Un piège à machoire
- Potion inconnue du tombeau du roi maudit x3
- Carte de Visite de Ekou
- Collier en dent d'orcs
- Orbe d'obsidienne Antique
	  Orbe venant d'Embala
	  Un genre de "moteur magique"