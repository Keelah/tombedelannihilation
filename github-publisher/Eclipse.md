---
share: "true"
---

Membres : 

- [Tonerre Poudrefine](./Tonerre%20Poudrefine.md)
- [Foudre](./Foudre.md) (ded)
- [Brann](./Brann.md)
- [Charles](./Charles.md)
- [Valdryzel Oakenhoof](./Valdryzel%20Oakenhoof.md)
- [Shaxx](./Shaxx.md)
- [Carmilla](./Carmilla.md)

Nouveau Chef : [Princesse Animale](./Princesse%20Animale.md)