---
share: "true"
---

**Race :** Liche 

Soignait les malades

Sauf que non, elle veut controler la ville

#### Son Plan : 
1. donner de la Drogue Rouge aux pauvres
2. Faire semblant de les soigner
3. En fait les transformer en zombies
4. Claquer des doigts et envahir la ville avec son armée de zombies

#### Sauf que : 
1. [[Ellie]], [[Tonerre Poudrefine]] et [[Valdryzel Oakenhoof]] s'en sont rendu compte
2. [[Ellie]] a déjoué son plan
3. On a fait tomber une maison sur sa gueule
4. CHEH

TO BE CONTINUED